public class TV {
    final int MAX_CHANNEL_COUNT = 3;
    private char name[];
    public Channel channels[];
    private int count;
    private RT rt;

    TV(char name[]){
        this.name=name;
        this.channels = new Channel[MAX_CHANNEL_COUNT];
        this.count=0;
    }

    void AddChannel(Channel channel) {
        if (count<MAX_CHANNEL_COUNT) {
            this.channels[count]=channel;
            this.count++;
            channel.setTV(this);

        }
    }




    public void setRT(RT rt) {
        this.rt = rt;
    }
}
