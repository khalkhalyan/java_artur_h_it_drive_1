public class Main {

    public static void main(String[] args) {
        TV tv = new TV("LG".toCharArray());

        Channel  channel1 = new Channel(1,"ORT".toCharArray());
        Channel  channel2 = new Channel(2,"RTR".toCharArray());
        Channel  channel3 = new Channel(3,"NTV".toCharArray());

        Programm programm1 = new Programm("Доброе утро".toCharArray(),"morning".toCharArray(), channel1);
        Programm programm2 = new Programm("Кармелита".toCharArray(),"afternoon".toCharArray(), channel1);
        Programm programm3 = new Programm("Вечерний Ургант".toCharArray(),"evening".toCharArray(), channel1);

        Programm programm4 = new Programm("Утро России".toCharArray(),"morning".toCharArray(), channel2);
        Programm programm5 = new Programm("Малахов+".toCharArray(),"afternoon".toCharArray(), channel2);
        Programm programm6 = new Programm("Ментовские войны".toCharArray(),"evening".toCharArray(), channel2);

        Programm programm7 = new Programm("Утро Самое лучшее".toCharArray(),"morning".toCharArray(), channel3);
        Programm programm8 = new Programm("Мухтар".toCharArray(),"afternoon".toCharArray(), channel3);
        Programm programm9 = new Programm("Воронины".toCharArray(),"evening".toCharArray(), channel3);

        RT rt = new RT();

        tv.setRT(rt);
        rt.setTV(tv);

        tv.AddChannel(channel1);
        tv.AddChannel(channel2);
        tv.AddChannel(channel3);

        channel1.addProgramm(programm1);
        channel1.addProgramm(programm2);
        channel1.addProgramm(programm3);

        channel2.addProgramm(programm4);
        channel2.addProgramm(programm5);
        channel2.addProgramm(programm6);

        channel3.addProgramm(programm7);
        channel3.addProgramm(programm8);
        channel3.addProgramm(programm9);

        tv.ShowChannel(2);





        // write your code here
    }
}
