public class Channel {
    private int number;
    private char name[];
    private Programm programms [];
    private TV tv;
    final int MAX_PROGRAMM_COUNT=3;
    private int count;

    Channel(int number, char name[]) {
        this.number = number;
        this.name = name;
        this.programms = new Programm[MAX_PROGRAMM_COUNT];
        this.count = 0;
    }

    public void setTV(TV tv) {
        this.tv = tv;
    }

    void addProgramm(Programm programm) {
        if (count<MAX_PROGRAMM_COUNT) {
            programms[count] = programm;
            count++;
        }

    }

}
